import math
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from Koordinatko.models import PointsPair


def index_view(request):
    points = PointsPair.objects.all().order_by("-id")

    def GET_view(request):
        # If a http param 'remove_id' is passed to the request
        # meaning user wants to delete an entry
        if request.GET.get("remove_id", None):
            try:
                pair = PointsPair.objects.get(id=request.GET["remove_id"])
                pair.delete()
                return HttpResponseRedirect("/")
            except Exception:
                print("Error while deleting entry with id", request.GET["remove_id"])
        return render(request, 'home.html', {"points" : points})

    def POST_view(request):
        x1 = request.POST.get("x1", None)
        y1 = request.POST.get("y1", None)
        x2 = request.POST.get("x2", None)
        y2 = request.POST.get("y2", None)

        # Check if all fields are filled out
        if not (x1 and y1 and x2 and y2):
            return render(request, 'home.html', {"points" : points, "message" : "Obvezno je izpolniti vsa vnosna polja!"})

        # Check if all fields are floats
        try:
            x1 = float(x1)
            y1 = float(y1)
            x2 = float(x2)
            y2 = float(y2)
        except ValueError:
            return render(request, 'home.html', {"points" : points, "message": "Dovoljen je samo vnos števil. Za decimalke se naj uporabi pika."})

        # Calculate the distance between two points
        distance = math.sqrt((x2-x1)**2 + (y2-y1)**2)

        # Save data
        pp = PointsPair.objects.create(x1=x1, y1=y1, x2=x2, y2=y2, distance=distance)
        pp.save()

        return HttpResponseRedirect("/")

    def error_view(request):
        return HttpResponse("The http method", request.method, "is not supported.")

    response = {
        "GET": GET_view,
        "POST": POST_view,
    }
    return response.get(request.method, error_view)(request)