from django.db import models

# Create your models here.
class PointsPair(models.Model):
    x1 = models.FloatField(null=False)
    y1 = models.FloatField(null=False)
    x2 = models.FloatField(null=False)
    y2 = models.FloatField(null=False)
    distance = models.FloatField(null=False)